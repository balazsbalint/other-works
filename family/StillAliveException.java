package family;

public class StillAliveException extends Exception {
    private static final long serialVersionUID = 1L;

	public StillAliveException(String msg) {
        super(msg);
    }

    public StillAliveException() {
        super("This person is still alive!");
    }

    public StillAliveException(Person member) {
        super(member.name() + " is still alive!");
    }

}