package family.test;

import family.*;

public class Main {
    public static void main(String[] args) {
        FamilyTree grandgrandfather = FamilyTree.create("Edward", 1914, 1988);
        FamilyTree grandmother = FamilyTree.create("Susan", grandgrandfather, null, 1950, 2016);
        FamilyTree grandfather = FamilyTree.create("John", 1940, 2004); // grandgrandparents are unknown
        FamilyTree mother = FamilyTree.create("Mary", grandfather, grandmother, 1980);
        FamilyTree father = FamilyTree.create("Jack", 1940); // grandparents are unknown
        FamilyTree peter = FamilyTree.create("Peter", father, mother, 1977);
        FamilyTree william = FamilyTree.create("William", grandfather, grandmother, 1974);

        System.out.println("Mother and William are siblings: " + mother.isSibling(william));
        System.out.println("Peter is related to William: " + peter.isRelativeOf(william));
    }
}