package family;

import java.util.Collection;
import java.util.LinkedList;
import java.util.Objects;

import family.StillAliveException;

public class FamilyTree implements Person {
    protected final String name;
    protected final FamilyTree father;
    protected final FamilyTree mother;
    protected final int birth;

    FamilyTree(String name, FamilyTree father, FamilyTree mother, int birth) {
        if (null == name) {
            throw new IllegalArgumentException("Name cannot be null!");
        }
        this.name = name.trim();
        if (name.isEmpty()) {
            throw new IllegalArgumentException("Name cannot be empty!");
        }
        this.father = father;
        this.mother = mother;
        this.birth = birth;
    }

    FamilyTree(String name, int birth) {
        this(name, null, null, birth);
    }

    @Override
    public String name() {
        return name;
    }

    @Override
    public String nameOfFather() {
        if (null == father) {
            return null;
        }
        return father.name();
    }

    @Override
    public String nameOfMother() {
        if (null == mother) {
            return null;
        }
        return mother.name();
    }

    @Override
    public int birth() {
        return birth;
    }

    @Override
    public String toString() {
        return name + ", " + birth;
    }

    public boolean isSibling(FamilyTree other) {
        return father == other.father && mother == other.mother;
    }

    private Collection<FamilyTree> relatives() {
        LinkedList<FamilyTree> list = new LinkedList<>();
        list.add(this);
        if (father != null) {
            list.addAll(father.relatives());
        }
        if (mother != null) {
            list.addAll(mother.relatives());
        }

        return list;
    }

    public boolean isRelativeOf(FamilyTree other) {

        Collection<FamilyTree> relatives = relatives();
        relatives.retainAll(other.relatives());
        return !relatives.isEmpty();
    }

    @Override
    public boolean equals(Object other) {
        return this == other;
    }

    @Override
    public boolean isAlive() {
        return true;
    }

    @Override
    public int death() throws StillAliveException {
        throw new StillAliveException(this);
    }

    public static FamilyTree create(String name, FamilyTree father, FamilyTree mother, int birth) {
        return new FamilyTree(name, father, mother, birth);
    }

    public static FamilyTree create(String name, int birth) {
        return new FamilyTree(name, birth);
    }

    public static FamilyTree create(String name, FamilyTree father, FamilyTree mother, int birth, int death) {
        return new FamilyTreeDead(name, father, mother, birth, death);
    }

    public static FamilyTree create(String name, int birth, int death) {
        return new FamilyTreeDead(name, birth, death);
    }
}