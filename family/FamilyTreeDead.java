package family;

class FamilyTreeDead extends FamilyTree {
    protected final int death;

    FamilyTreeDead(String name, FamilyTree father, FamilyTree mother, int birth, int death) {
        super(name, father, mother, birth);
        if (birth > death) {
            throw new IllegalArgumentException(String.format("Invalid birth(%1)/death(%2) year!", birth, death));
        }
        this.death = death;
    }

    FamilyTreeDead(String name, int birth, int death) {
        this(name, null, null, birth, death);
    }

    @Override
    public boolean isAlive() {
        return false;
    }

    @Override
    public int death() throws StillAliveException {
        return death;
    }

    @Override
    public String toString() {
        return super.toString() + "-" + death;
    }
}