#ifndef ARSHIFT_H_INCLUDED
#define ARSHIFT_H_INCLUDED
#include <vector>

class array_shifter{
private:
    int arrsize;
    int* actual;
    std::vector<int> original;
public:
    array_shifter(int be[], int meret)
    {
        this->arrsize = meret;
        this->actual = be;
        for(int i=0; i<meret; ++i)
        {
            original.push_back(be[i]);
        }
    }
    int size() const
    {
        return arrsize;
    }
    void operator>>(int szam)
    {
        int counter = 0;
        while(counter != szam)
        {
            int last = actual[arrsize-1];
            for(int i= arrsize-2; i>=0; --i)
            {
                actual[i+1] = actual[i];
            }
            actual[0] = last;
            ++counter;
        }
    }
    void operator<<(int szam)
    {
        int counter = 0;
        while(counter != szam)
        {
            int first = actual[0];
            for(int i= 0; i<(arrsize-1); ++i)
            {
                actual[i] = actual[i+1];
            }
            actual[arrsize - 1] = first;
            ++counter;
        }
    }
    ~array_shifter()
    {
        for(int i= 0; i<arrsize; ++i)
        {
            actual[i] = original[i];
        }
    }
};

template<class T>
class array_shifter_util{
private:
    int arrsize;
    T* actual;
    std::vector<T> original;
public:
    array_shifter_util(T be[], int meret)
    {
        this->arrsize = meret;
        this->actual = be;
        for(int i=0; i<meret; ++i)
        {
            original.push_back(be[i]);
        }
    }
    int size() const
    {
        return arrsize;
    }
    void operator>>(int szam)
    {
        int counter = 0;
        while(counter != szam)
        {
            T last = actual[arrsize-1];
            for(int i= arrsize-2; i>=0; --i)
            {
                actual[i+1] = actual[i];
            }
            actual[0] = last;
            ++counter;
        }
    }
    void operator<<(int szam)
    {
        int counter = 0;
        while(counter != szam)
        {
            T first = actual[0];
            for(int i= 0; i<(arrsize-1); ++i)
            {
                actual[i] = actual[i+1];
            }
            actual[arrsize - 1] = first;
            ++counter;
        }
    }
    ~array_shifter_util()
    {
        for(int i= 0; i<arrsize; ++i)
        {
            actual[i] = original[i];
        }
    }
};

///Az array_shifter oszt�ly teljes eg�sz�ben t�r�lhet�, az al�bbi typedef is b�ven el�g:
//typedef array_shifter_util<int> array_shifter;

#endif // ARSHIFT_H_INCLUDED
