/* 
Írjunk meg egy PL/SQL procedúrát, amelyik egy gyakoriság szerinti fektetett (vagy álló)
   oszlopdiagrammot ír ki a képernyőre a paraméterül kapott tábla adott oszlopában előforduló
   értékek előfordulási gyakorisága alapján. A procedúra paramétere egy felhasználónév, egy 
   táblanév és egy oszlopnév (a táblának az egyik oszlopa). A procedúrát dinamikus SQL
   utasítások használatával célszerű megírni, de akinek ez nehezen megy, érdemes előtte 
   úgy megírni, hogy egy rögzített táblára és oszlopra működjön. Az oszlopok hosszúságát
   érdemes az előforduló gyakoriságok alapján arányosan kirajzolni, ha túl nagy gyakoriságok
   szerepelnek, akkor valamilyen tényezővel elosztani. Az output valami olyasmi legyen, mint 
   ami az alábbi példákban látható. Minimális hibakezelés is legyen a programban, például
   nem létező táblára vagy oszlopra írja ezt ki. Ha túl sok különböző érték fordul elő
   az oszlopban (>100), akkor a program írja ki, hogy ez túl sok, és ilyenkor nem kell
   kirajzolnia az oszlopokat.
*/

/*
Készítette: Balázs Bálint

*/
CREATE OR REPLACE PROCEDURE print_histogram(p_owner VARCHAR, p_table VARCHAR, p_col VARCHAR) 
IS
    curs_str VARCHAR2(400); 
    query1 VARCHAR2(400);
    query2 VARCHAR2(400);
    tableExist VARCHAR2(400);
    columnExist VARCHAR2(400);
    TYPE CurTyp  IS REF CURSOR;
    curs CurTyp;
    distrowcount NUMBER(28);
    tableCount NUMBER(1);
    columnCount NUMBER(1);
    v_row VARCHAR2(200);
    numOfRows NUMBER(28);
begin 
    curs_str := 'select distinct '||p_col||' from '||upper(p_owner)||'.'||upper(p_table)||' where '||p_col||' is not null order by '||p_col;
    query1 := 'select count(distinct '||p_col||') from '||upper(p_owner)||'.'||upper(p_table)||' where '||p_col||' is not null';
    tableExist := 'select count(*) from dba_tables where owner = :x and table_name = :y';
    EXECUTE IMMEDIATE tableExist into tableCount using upper(p_owner),upper(p_table);
    if tableCount > 0 then
        columnExist :=  'select count(*) from dba_tab_columns where owner = :x and table_name = :y and column_name = :z';
        EXECUTE IMMEDIATE columnExist into columnCount using upper(p_owner),upper(p_table),upper(p_col);
        if columnCount > 0 then
            EXECUTE IMMEDIATE query1 into distrowcount;
            if distrowcount < 100 then
                    open curs for curs_str ;
                    loop
                        FETCH curs INTO v_row;
                        query2 := 'select count(*) from '||upper(p_owner)||'.'||upper(p_table)||' where '||p_col||'= :x group by '||p_col;
                        EXECUTE IMMEDIATE query2 into numOfRows USING v_row;
                        numOfRows := numOfRows/50;
                        EXIT WHEN curs%NOTFOUND;
                        dbms_output.put_line(v_row||'   -->   '||lpad(' ',numOfRows,'*'));
                    end loop;
                    close curs;
            else
                dbms_output.put_line('Túl sok különböző érték fordul elő a táblában!!!!!!');
            end if;
        else
             dbms_output.put_line('Nem létezik az oszlop!!!');
        end if;
    else
        dbms_output.put_line('Nem létezik a tábla!!!');
    end if;
end;

set serveroutput on;
CALL print_histogram('nikovits','test2','yr');

/*futtatás eredménye:
1961   -->   ******************** 
1962   -->   ******************* 
1963   -->   ******************* 
1964   -->   ******************* 
1965   -->   ****************** 
1966   -->   ****************** 
1967   -->   ****************** 
1968   -->   ****************** 
1969   -->   ****************** 
1970   -->   ****************** 
1971   -->   ****************** 
1972   -->   ***************** 
1973   -->   ***************** 
1974   -->   ***************** 
1975   -->   ****************** 
1976   -->   ***************** 
1977   -->   ***************** 
1978   -->   ****************** 
1979   -->   ****************** 
1980   -->   ***************** 
1981   -->   **************** 
1982   -->   ************** 
1983   -->   ************* 
1984   -->   ********** 
1985   -->   ****** 
1986   -->   ***** 
1987   -->   ** 
1988   -->   * 
1989   -->    
1990   -->   */